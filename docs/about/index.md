# About me

![](../images/myAvatar.png)

Hi! I am Osmel Martínez Rosabal. I am a research assistant at the Centre of Wireless Communications, here at the University of Oulu.

## My background

I was born in a Caribean island called Cuba, a very hot country full of music and great people. 

## Previous work

After my graduation, I started a job in a telecommunication company, where mainly fixed services where provided. After five years of work there, I decided to move to the research world where everything is possible. That's the reason I came to the place with one of the highest quality education system in the world: Finland, to do my Master in Wireless Communications.

# 2. Interface and application programming

This week I worked on writing applications that interface an user with the Arduino UNO board and the ATtiny412-based board made in the [week 1](https://osmel-dev.gitlab.io/fablab-project/assignments/week01/).

- [Group Assignment 2 (Interface and application programming)](https://dechevar19.gitlab.io/gitlab-project/assignments/week17/)
The goal for this assignment was to compare as many tool options as possible for creating Graphical User Interfaces. Using Arduino UNO as the hardware platform, we programmed our interfaces with Processing 3, Python (Tkinter), and MatLab (GUIDE). The application was simple, a servo motor is rotated 90 degrees clockwise and counterclockwise according to the pressed button in the interface.

## CO2 meter application using Arduino UNO

Research
The current levels of CO2 in our environment are mostly produced by the human activity, say transportation, industries, etc. No matter the source from which the CO2 is comming, the impact on the human being quality of life is noadays a big problem. For that, I would like to present a simple application, made in Processing, that shows the current level of CO2 read from a SGP30 air quality sensor connected to an Arduino UNO. The main hardware components are shown below. 

![](../images/interfaceProgramming/SGP30.jpg)
![](../images/interfaceProgramming/ArduinoPinout.jpg)

We also capture snapshots of the IDEs that we were using:

Processing
![](../images/interfaceProgramming/ProcessingIDE.jpg)

Arduino
![](../images/interfaceProgramming/ArduinoIDE.jpg)

Useful links
- [Processing](https://processing.org/)
- [Tkinter](https://docs.python.org/3/library/tkinter.html)
- [Java](https://code.visualstudio.com/docs/languages/java)
- [ArduinoUNO](https://components101.com/microcontrollers/arduino-uno)

The Application
The figure shows the hardware connection between the arduino and the sensor. 
![](../images/interfaceProgramming/Arduino_UNO_CO2Sensor.jpg)

The protocol for connecting the air quality sensor to the Arduino is the I2C. I2C is a multi-master and multi-slave serial bus protocol for connecting low-speed peripherals to microcontrolers, within short distances. It is implemented just by using two lines: Serial Data Line (SDA) and Serial Clock Line (SCL), and each device connected to the bus is referenced using a 7-bit address. The figure below depicts an exemplary connection with a master microcontroler and three slave devices: Digital-to-Analog Converter, Analog-to-Digital Converter, and a microcontroler.

![](../images/interfaceProgramming/I2C.jpg)

For programming the Graphical User Interface (GUI) we use Processing 3 and the library ControlP5 for creating the widgets. After creating an instance of the ControlP5 object, a slider is added to the GUI. Additional parameters are provided for define the position, size, range, and the number of ticks of the slider. The slider changes its value as a function of the level of CO2. We distinguish three levels: red (normal), yellow (warning), red (dangerous), for the intervals 400ppm < CO2 < 1000ppm, 1000ppm < CO2 < 2000ppm, and 2000ppm < CO2 respectively.

![](../images/interfaceProgramming/CO2Meter.jpg)

Processing Code
```
// GUI interface for the Arduino UNO serial communication.
import processing.serial.*;
import controlP5.*;

ControlP5 cp5;
int slider = 400;
Serial myPort;
String str = "";
int myColorBackground = color(255, 255, 0);

void setup() {
  size(250, 400);  
  cp5 = new ControlP5(this);
  PFont GUIfont = createFont("arial", 20);
  cp5.setFont(GUIfont);
  
  cp5.addSlider("CO2 Level").
  setPosition(50,50).
  setSize(100,300).
  setRange(400, 5000)
  .setNumberOfTickMarks(10);
  
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);
  myPort.bufferUntil('\n');
}

void draw() {  
  background(0);
    
  if (str != null){
    float val = float(str);
    cp5.getController("CO2 Level").setValue(val);
    
     // Chagge the color of the slider as a function of the CO2 level
    if (val < 1000){
      cp5.getController("CO2 Level").setColorForeground(color(0,250,0));
    } else if(val > 1000 && val < 2000){
      cp5.getController("CO2 Level").setColorForeground(color(255,255,0));
    } else {
      cp5.getController("CO2 Level").setColorForeground(color(255,0,0));
    }

  }
}

// Serial port interruption
void serialEvent(Serial port) {
  str = port.readStringUntil('\n');
} 
```

Notice that everything happens within two main functions: setup() and draw(). The statements within the body of the setup() function 
are execute once when the program begins, mainly for general purpose configurations. In our case we define the appearance and widgets of the GUI, and the serial port used for communicating with the Arduino. Whereas all the statements withing the body of the draw() function are executed until the program ends. Herein we continuously read the measurements sent via serial port and display the values using the slider.

Arduino code
```
#include <Wire.h>
#include "Adafruit_SGP30.h"

Adafruit_SGP30 sgp;

void setup() {
  //initialize serial communications at a 9600 baud rate
  Serial.begin(9600);  
  sgp.begin();
}

void loop() {
  sgp.IAQmeasure();
  Serial.println(sgp.eCO2);
  delay(1000);
}
```

For handling the SGP30 air quality sensor, we use the library Adafruit_SGP30.h provided [here](https://github.com/adafruit/Adafruit_SGP30). Then the serial port is initialized to match with the configuration in the GUI interface. The readings are taken using the function IAQmeasure(), which returns, among others, the CO2 level. As it is shown in the video the application is tested by blowing air to directly the sensor.

## ON/OFF led application using the ATtiny412-based board
This application shows the communication of a user interface Processing 3 application and the ATtiny412-based board designed in [week 1](https://osmel-dev.gitlab.io/fablab-project/assignments/week01/). The board includes one push button, a led, a programming interface, and a Future Technology Devices International (FTDI) female connector for serial communication. At the moment this application was built, the access to FabLab's premises was restricted due to COVID-19, then I decided to use the available components at home. Specifically, I turned the Arduino UNO in a proxy between the PC and the board.

Connection: the connection is very simple. We use the UART pins (0, 1) of the Arduino UNO for communicating with the board. Therefore we have to code our application for three different nodes: Arduino UNO and ATtini412 board (using the Arduino IDE), and the PC application (using Processing 3).  

Arduino UNO code
```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(0, 1); // RX, TX

void setup() {
  mySerial.begin(9600);
  pinMode(3, INPUT);
}

void loop() {} // Just "forward" data
```

As we comment in the code, the Arduino is utilized for making possible the connection with the board. 

ATtiny412 board code
```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3); // RX, TX

const char OFFSTATE = '1'; // led off constant
const char ONSTATE = '2'; // led on constant
const int led =  1; // the number of the LED pin

void setup() {
  mySerial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(3, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {    
    char receivedByte = mySerial.read();
    
    if (receivedByte == ONSTATE) {      
      digitalWrite(led, HIGH); // turn on the led
    }

    if (receivedByte == OFFSTATE){
      digitalWrite(led, LOW); // turn off the led
    }
  }
}
```
Herein the SoftwareSerial.h library is used for the serial communication. [Here](https://www.arduino.cc/en/Reference/softwareSerial) is the reference documantation and some examples. Noteice that, whenever the board receives a command it checks for the next state of the LED: "ON" or "OFF".

Processing code
```
// GUI interface for the ATtiny412 serial communication.
import processing.serial.*;
import controlP5.*;

ControlP5 gui;
Serial myPort;

void setup(){
//set the window size
size(200,200);
noStroke();
//Create the new GUI
gui = new ControlP5(this);
PFont GUIfont = createFont("arial", 20);
  gui.setFont(GUIfont);
  
// Add the "ON" Button
gui.addButton("ON")
   //Set the position of the button : (X,Y)
   .setPosition(50,30)
   //Set the size of the button : (X,Y)
   .setSize(100,50)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
   
// Add the "OFF" Button
gui.addButton("OFF")
   //Set the position of the button : (X,Y)
   .setPosition(50,100)
   //Set the size of the button : (X,Y)
   .setSize(100,50)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
   
   String portName = Serial.list()[2];
   myPort = new Serial(this, portName, 9600);
}

public void ON(int value){
// activated by the "ON" Button and print the current led's state
println("ON"); 
myPort.write('2');
}

public void OFF(int value){
// activated by the "OFF" Button and print the current led's state
println("OFF");
myPort.write('1');
}

public void controlEvent(ControlEvent theEvent) {}

void draw(){}
```
Here we have added two bottons namely ON and OFF for controlling the LED in the board. Each of them has an associated function that sends a commmand via serial port. For instance, the ON button points out to the function public void ON(int value) which prints the state of the led and sends a command for turning it on in the board. The figure below shows a tipical data frame for a serial commubication. Notice that our commands will be sent within the "Data" slots.

![](../images/interfaceProgramming/TypicalDataFrame.jpg)

Basically, the GUI sends two characters: i) '1' for turning the led "OFF"; ii) '2' for turning the led "ON". Hence, the comand is sent as a char using ASCII encoding.

## Link to the codes
CO2 meter

- [Arduino UNO sketch](../files/ARDUINO_CO2.ino)

- [Processing 3 sketch](../files/CO2_meter.pde)

ON/OFF led

- [Arduino UNO sketch](../files/ARDUINO_UNO_GUI.ino)

- [ATtiny412-based board sketch](../files/ATtiny412_GUI.ino)
 
- [Processing 3 sketch](../files/ON_OFF_LED.pde)

## Problems discussion
Special attention must be taken to the Serial Port pins defined by the object myport, since they change according to the board.

## Videos

CO2 meter
<iframe src="https://player.vimeo.com/video/453961733" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

On/OFF LED
<iframe src="https://player.vimeo.com/video/453962672" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

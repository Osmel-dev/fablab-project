# 3. Applications and implications

The target for this week is to end up with a project proposal, including the hardware and the tools to develop it.

## The Project: Smart agriculture at home.

With the continuous growing of the world population, the food production is claiming for more nobel and effective techniques to get it with high quality, and to make it available regardless of the weather conditions. Our project is about growing the food at home using the potentials of the embedded systems to make the process full automated and accurate enough. A single instance of the project is not intended for large production, the main idea is to get it a per home instance, so that every family can harvest their own, that is a simple idea with a big impact. This idea also applies for ornamental plants

## Who has done what beforehand?

When you google this idea, there are tons of projects from large to medium scale to grow crops, but making at your own has the benefit of fully customizable design that can be accommodated to your specific conditions. In the FabLab website there are also similar projects, but what makes this different is the use of the collected data to improve the farming.

In my research I found an [Hydroponic project](http://archive.fabacademy.org/2018/labs/fablaboulu/students/ari-vuokila/finalProject.html) where the guys control the pH, nutrients, and fertilizers in the water tank for feeding the plants. A good theoretical background of the roll of IoT in smart farming is disussed [here](https://www.mdpi.com/1424-8220/20/15/4231/htm). They also studied some tools for the data processing such as machine learning, deep learning, and computer vision. Finally, [here](https://www.agritecture.com/blog/2020/1/31/the-smart-agriculture-system-making-commercial-and-home-farming-easier) hydroponics for commercial and home farming are designed. They also provide consulting and design services. 

## Project description

Here is an sketch of the idea.
![](../images/FabLab_Project.jpg)

The number of components needed for this project will depend on the size of the project itself, so we propose to define the smallest useful module (called here as farming cell) that can build the whole system. Within the farming cell, we should control the environmental conditions according to the specific crop, which support the idea of independent modules and makes the process more efficient. For instance, there are crops such as lettuce, many herbs and salad greens that have an optimum temperature range of approx 16 – 22 C whereas tomatoes, capsicum, cucumbers have a higher range of 18 – 28 C. The same applies for humidity: between 50% to 70% in vegetative growth, and 50% to 60% for flowering plants. The ATtiny412-based board devices will collect data related with the light intensity, humidity, temperature, and CO2 level, and it will send them to a central processing (a Rasberry Pi in our case) unit via seial communication. Each board(s) will implement(s) the I2C compatible protocol in order to communicate with the sensors. The nutrients for the plants will be stored in water containers which will facilitate the dosage, and we propose the use of both artificial and ambient light, which needs the farming cells to have surfaces with high penetration of the sunlight, such as the acrylic. The use of the light is illustrated in an example: during the summer we can take advantage of the sunlight, but in the winter time or depending of the location of the farming cells artificial lights could be needed with a timer controlling the intensity according with the natural daily schedule, using for instance a dimmer.

A heater system will be in charge of kepeing the temperature inside the farming cell appropriate with the type and the stage of the crop, and the water can be spread using small electronic pumps already available in the market, or electronic valves if we can take advantage of the gravity force. Most of the plants that grow in indoors require a minimum CO2 concentration of 330 mg/l to enable them to photosynthesise efficiently and produce energy in the form of carbohydrates. It can be supplied in different ways:

1) burn a fuel like natural gas or propane,

2) release CO2 from a tank at a given rate using a regulator,

3) use a decomposition process.

Therefore our farming cell must be able to meassure the level of CO2 and take the proper action considering the aforentioned approaches.

Finally, the Raspberry Pi, who is in charge to collects the measurements, can learn about the daily sunlight schedule and then optimize for instance the position of the plants according to the availability of the sunlight and the specific requirements, since we provide the profile for each specific crop. At a glance it does not look benefitial at all, but consider the case when you have spread all cells around and with different availability of sunlight, then the system will tell you how to reorder the cells according with the current conditions.

We can also use the computational power of the central unit to process pictures coming from cameras installed at the cells to determine if something goes wrong. For instance, after certain period of planting the seeds we expect to have some seedlings, and that is something we can check automatically using digital image processing. Moreover, the color of the leaves are a good indicator of its health; therefore, given a reference the Rasberry can check for plages or another kind of sickness. 

## What will you design?
- The process of getting the boards ready starts with a computed aided design (CAD), and after a few iterations we can get the needed files for creating the PCB using the Roland Milling Machine (SRM-20). The process ends with soldering, programming and partial tests to see if the borads work properly.
- Some accessories are possibly made using 3D printing process (water tank, supports for the pump, etc.).
- The frame parts (plywood) are built using the laser cutting process. All these parts will box our design as a single module. 

A good candidate CAD for 3D modeling is the [Autocad Fusion 360](https://www.autodesk.fi/products/fusion-360/subscribe?mktvar002=afc_fi_nmpi_ppc&AID=13355247&PID=8299312&SID=jkp_CjwKCAjwnef6BRAgEiwAgv8mQc7aC4jScNLMWuWNUdNwg_pICiKUwq2l4Xumk-DbBTZlD_ZCHJm_SxoCCQ4QAvD_BwE&click_id=CjwKCAjwnef6BRAgEiwAgv8mQc7aC4jScNLMWuWNUdNwg_pICiKUwq2l4Xumk-DbBTZlD_ZCHJm_SxoCCQ4QAvD_BwE&cjevent=5f9f541bf34f11ea82b100630a180513&ds_rl=1232386&gclid=CjwKCAjwnef6BRAgEiwAgv8mQc7aC4jScNLMWuWNUdNwg_pICiKUwq2l4Xumk-DbBTZlD_ZCHJm_SxoCCQ4QAvD_BwE&gclsrc=aw.ds&affname=8299312_13355247&plc=F360&term=1-YEAR&support=ADVANCED&quantity=1) which offers great tools for getting ready the design. Although I am not an expert in CAD design, I have some experience with some of the Autocad tools, mostly for 2D design. Additionally, I found [Blender](https://www.blender.org/) which is an open source 3D creation suite.   

## List of materials and components
The list below show the list of needed materials and component for this project, as well as the approximate price in euros. I have conducted a search in the internet for checking the prices in different online stores, and almost all components are linked to the page where I found it. At the end I have came up with an approximation of the total cost.

- 1x Raspberry PI 3 (FabLab): --> Price: 79.99
- 3x ATtiny412 (FabLab): --> Price: 0.42 per unit
- 1x [SGP30: Temperature Sensor, CO2 Sensor, Humidity Sensor](https://www.adafruit.com/product/3709) --> Price: 19.95 
- 1x [Camera](https://www.24hshop.fi/tietokonetarvikkeet/tietokonetarvikkeet/kamera-modul-raspberry-pi-5mp-1080p?gclid=CjwKCAjw4rf6BRAvEiwAn2Q76vEF2YPj95li6SZq6EQstGGZwnn6oIqKJrpXFuWSXLPhNsYIWrifhBoC8PMQAvD_BwE) --> Price: 17.90
- 1x [Mini water pump](https://aliexpress.ru/item/4001265106888.html?spm=a2g0o.productlist.0.0.51b24e32pMODCU&algo_pvid=34abddde-6c20-4126-9a2e-4bed2e571e22&algo_expid=34abddde-6c20-4126-9a2e-4bed2e571e22-3&btsid=0ab6fab215989640105393847e1c69&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_) --> Price: 17.18
- 1x Power Supply
- 1x [Eletrovalve (to supply the CO2)](https://www.alibaba.com/product-detail/irrigation-solenoid-valve-low-voltage-solenoid_2001739633.html?spm=a2700.7724857.videoBannerStyleB_top.1.134b7fdcHadqei) --> Price: 1.50
- [Led lights (proper for indoor agriculture)](https://www.alibaba.com/product-detail/Agricultural-Led-Light-Led-K1000-Amazon_62035073462.html?spm=a2700.7724857.normalList.2.1fd716646cj9hl) --> Price: 38.38
- Plywood (FabLab)
- Acrylic sheets (FabLab)
- [Plastic pipes](https://www.ebay.co.uk/itm/PVC-Braided-Pipe-Hose-Tubing-Irrigation-Water-Ponds-Koi-DIY-5m-10m-30m-Coils/121901515532?_trkparms=aid%3D1110006%26algo%3DHOMESPLICE.SIM%26ao%3D1%26asc%3D225114%26meid%3Db10d0f34feeb4db08fdda003cebe5ccd%26pid%3D100005%26rk%3D1%26rkt%3D12%26mehot%3Dpf%26sd%3D353099629556%26itm%3D121901515532%26pmt%3D1%26noa%3D0%26pg%3D2047675%26algv%3DSimplAMLv5PairwiseWebWithBBEV2bDemotion%26brand%3DUnbranded&_trksid=p2047675.c100005.m1851) --> Price: 1.80 per m.
- Additional electronics (resistors, capacitors, wires, batteries, etc.) (FabLab)

Total estimated cost: 180.00 euros.

## How will it be evaluated?
- Each sensor reading must be double checked with a dedicated instrument.
- By forcing the physical variables, we can see the system response. This will ensure the minimum funcionality of the system: "It must react to any change in the physical variables that requires an action". For instance, if too much CO2 is comming the valve must be closed.
- Ensure that the system works for a time period without falling into endless loops or unstable conditions. Check the operation temperature of each component. In case of continuous and long operation periods some components might be exposed to high power consumption that can cuases overheating. In these tests, the time interval can be chosen as 8-10h.

## What questions need to be answered?
- Is it affordable?
- How to connect/power/use a water pump?
- What is the minimum size of the water tank needed?
- How to program a Machine Learnig algorithm into the Rasberry Pi 3? What platform should I use? Is there any code/library ready? 
- Are those the only parameters to measure for efficiently growing the crops?
- How to efficiently place the sensors?
- What is the minimum information (image) for determining the health of a crop?

## What have I learned?
- The process of creating a project from scratch: from research to implementation.
- The principles of embedded systems and how to use some of the available tools.
- Digital fabrication processes. Although I haven't gone throughout all of them, I found a lot of different possibilities of realize my ideas.
- I have gained experience in the challengeing and sometimes long process of troubleshooting. Sometimes a simple mistake takes hours to be discovered. So, be patient and start again from the beginning to see what went wrong.

## Additional references

- [IoT in Agriculture: Five Technology Uses for Smart Farming and Challenges to Consider](https://dzone.com/articles/arm-offers-a-helpline-to-iot-startups)
- [Possible roles of MAchine Learning towards smart agriculture](https://medium.com/sciforce/machine-learning-in-agriculture-applications-and-techniques-6ab501f4d1b5)
- [Smart Agriculture: The future of agriculture](https://www.iotforall.com/smart-farming-future-of-agriculture/)
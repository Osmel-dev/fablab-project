# 1. Embedded programming

The aims of this week were to program the Olimex board to do something, with as many different programming languages and programming environments as possible. Additionally, we were told to make our own board based on the ATtiny-412, to then implement a very simple application. The description is divided in two sections, the first will deal with the Olimex board implementation whereas the second will be about the ATtiny-412 based board. In both cases a brief description of the microcontrolers is included.

- [Group Assignment 1 (Embedded programming)](https://nelsongi.gitlab.io/nelsonproject/assignments/week19/)
The target of this assignment is to compare the performance and development workflows for other architectures. Herein, we describe two architectures: Von Neumann and Hervard.

# Part I: Implementation with the Olimex board
## Datasheet
AVR-MT128 is a board which uses the as MCU, the ATMega128 from Atmel. It has a variety of peripherals such that LCD, buttons, relay and variety of communication interfaces such as RS232, JTAG, etc., which make this board suitable for different embedded systems applications. 
![](../images/embeddedProgramming/ATMEGA128.jpg)

## Useful links
The links listed bellow can be checked to compare the pinout provided by Olimex and MegaCore. Also, the functions to program the AVR-MT128 board using the Arduino IDE can be found in the reference page.

- [Olimex](https://www.olimex.com/Products/AVR/Development/AVR-MT128/resources/AVR-MT128.pdf)
- [MegaCore](https://github.com/MCUdude/MegaCore)
- [Arduino](https://www.arduino.cc/reference/en/)

## Procedure
The programer was an AVR-JTAG-USB (Olimex programmer), to download the code from the Arduino IDE into the AVR-MT128 board. A blinking led program was used, just to get familiar with the whole process. I would like to highlight some issues that probably the people less familiar with communication could face: the communication is made throuh a RS232 port (JTAG in the board), so that it should be recognize with a specific name depending on the operating system; the IDE must be configure for the Olimex programmer, and it is shown in the figure bellow (Tools); and finally the pins that will be in use must be taken as they are in MegaCore, but using the schematic of the board.
![](../images/embeddedProgramming/config.jpg)

## The code
The program is very simple. Every time the Button1 is pressed the internal state is modified, so that the led can blink (1 Hz) or nor. Meanwhile, the current state (Led off! - Led on!) is shown in the LCD.

```
#include <LiquidCrystal.h>
// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 28, en = 30, d4 = 32, d5 = 33, d6 = 34, d7 = 35;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// define Led pin and activation button
int led = 38;
int button1 = 44;

// to store the button state
int buttonState = 0;
int currState = 0;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(button1, INPUT);
  //Pin29 is R/W pin for LCD
  pinMode(29, OUTPUT);
  digitalWrite(29, LOW);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.setCursor(2, 0); // top left
  lcd.print("Blinking led!");
  delay(1500);
}
void loop() {
  buttonState = digitalRead(button1);
  
    if (buttonState == LOW){
      currState = !currState;   
      delay(250);
    }

    if (currState == 1){
      digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(500);              // wait for a second
      digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
      delay(500);              // wait for a second

      lcd.setCursor(4, 1); // top left
      lcd.print("Led On!");      
    } else{          
      lcd.setCursor(4, 1); // top left
      lcd.print("Led Off!");
    }    
}
```

## Video

<iframe src="https://player.vimeo.com/video/401400191" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

# Part II: Implementation with the ATtiny412.
## Datasheet
The ATtiny412 is an 8-pins microcontroler based on the high performance low-power AVR RISC architecture, which can be programmed by means of the Single Pin Unified Program Debug Interface (UPDI). It also includes Digital-to-Analog and Analog-to-Digital convertes, timers, counters, serial ports, among others peripherals, making it useful for a wide range of applications. It has three sleep modes:  idle with all peripherals running and mode for immediate wake up time, standby, and power down with wake-up functionality. Its block diagram is shown in the figure below.
![](../images/embeddedProgramming/ATtinyBlockDiagram.jpg)

## Procedure
For designing the board was used AUTODESK EAGLE and the library [eagle_fab.lbr](https://gitlab.cba.mit.edu/pub/libraries/-/tree/master/eagle) which contains a list of the elctronic components used in Fab Lab. The process of getting ready the board is briefly described bellow:

- At the beginning, the library and project paths must be provided to EAGLE, which is done by going to Options --> Directories...
![](../images/embeddedProgramming/Eagle.jpg)

- To create a new project, we go to the project folder and right click to find the option New Project.

- After that, the schematic and pcb files must be created as part of our project. The former describes the electrical connections among the different components, whereas the pcb file is made by the pysical instantiation of the components and its connections. 

Schematic view.
![](../images/embeddedProgramming/ATtinySch.jpg)

PCB View
![](../images/embeddedProgramming/ATtinyPCB.jpg)

- In this step, we print an image of the bottom copper layer and the outline to cut the board. Note that the milling tool will only remove the black regions. 
![](../images/embeddedProgramming/PCBprint.jpg)

- We used [MODS](http://mods.cba.mit.edu/) to get the .rml files form the .png figures previously generated. Herein, is configured the size of the tool, the depth of the cutting and the max number of iterations when taking off the copper. 
![](../images/embeddedProgramming/rolandfile.jpg)

- Hands on! With the .rml files we can now cut the board usind the SRM20 milling machine.
![](../images/embeddedProgramming/PCBprocess.jpg)

Here are the needed files for getting the board ready: i) [Eagle files (project, shcematic, and board)](../files/PCBfiles.zip); ii) [Milling files (board milling file and outline file)](../files/Millingfiles.zip).

In order to program the board, we turn an Arduino UNO into an UPDI programmer. The steps to get this can be found [here](https://www.electronics-lab.com/project/getting-started-with-the-new-attiny-chips-programming-the-microchips-0-series-and-1-series-attiny-with-the-arduino-ide/).

Finally we provide the list of electronic components and the final circuit
- 1x SMD 1k RESISTOR
- 1x SMD 10k RESISTOR
- 1X 1uF CAPACITOR
- 1x ATtiny412
- 1x Omron Switch (pushbutton)
- 1x SMD RA FEMALE connector (3 pins)
- 1x FTDI SMD HEADER (6 pins)

![](../images/embeddedProgramming/attiny.jpg)

## The code
The application makes the led blinking with a pseudo-random frequency (1~2.5 Hz) whenever the push button is pressed, and turn the led off otherwise.
```
const int buttonPin = 0;     // pushbutton pin
const int ledPin =  1;      // LED pin

// variables will change:
int buttonState = 0;         // pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  randomSeed(300); // initializes the pseudo-random number generator
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed.
  // if it is, make the led flash with a variable frequency.
  if (buttonState == LOW) {    
    randNumber = random(200, 500);
    digitalWrite(ledPin, HIGH);
    delay(randNumber);
    digitalWrite(ledPin, LOW);
    delay(randNumber);
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}
```

## Link to the codes
- [Olimex firmware](../files/OLIMEX.ino)
- [ATtiny412-based board](../files/ATtiny_LED.ino)

## Video

<iframe src="https://player.vimeo.com/video/436952732" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

# 4. Networking and communications

This week I worked on the communication between two arduino devices using the serial interface, also known as Universal Asynchronous Receiver Transmitter (UART).

- [Group Assignment 3 (Networking and communications)](https://henriquehilleshein.gitlab.io/olimex_pushbutton_led/assignments/week04/)
The objective here was to communicate two projects. The communication between boards is made through internet. A sensor located somewhere, sends information using Arduino UNO as its interface, whereas the alarm controlled device is implemented with the Olimex Board.

## Procedure

On the Arduino Uno platform the pins 0 and 1 are used for serial communications, wich can be monitored in the built-in tool in the Arduino environment. The serial communication on pins TX/RX uses TTL logic levels (5V or 3.3V depending on the board), and it should be kept in mind before conecting the board to another computer system. 

The UART takes individual bits and send them sequentially, it includes a start bit, a stopt bit, and parity check for the information bits. There is no information shared about the signal clock, so it should be recovered from the data stream at the receiver in order to reliable decode the information. At the end, the serial stream is converted to parallel by collecting the individual bits.


Here is the schematic of the system connection

![](../images/networking_communications/IMG_2173.JPG)

## Useful links

- [UART interface](https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter)
- [Arduino](https://www.arduino.cc/reference/en/language/functions/communication/serial/)

## The Code

This application uses the serial communications protocols for embedded system devices. The information is shared with a common port, but only the device that has been invoked, emits a signal. Each device takes on an address from the set {'A', 'B', ..., 'Z'}. Although we only use capital letters, the addressing can be even more complex, depending on the number of devices in the network. Once the requested device receives the message, it blinks a led 10 times. For our particular example, we only use two devices: an Arduino UNO and the ATtiny-412-based board manufactured for us in the first week. Here is the code

for the Arduino UNO;
```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(0, 1); // RX, TX

const char addr = 'A'; // network address
bool flag = false;
const int led =  13; // LED's pin

void setup() {
  mySerial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(3, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {    // check if it something available
    char receivedByte = mySerial.read();
    if (receivedByte == addr) {      
      flag = true;
    }
  }

  if (flag == true){  
    for (int i = 0; i < 10; i++) {    // flash the led 10 times
      digitalWrite(led, HIGH);
      delay(200);
      digitalWrite(led, LOW);
      delay(200);  
    }          

    flag = false;  // set the flag to false until the next valid request
  }
}
```

for the ATtiny-based board
```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3); // RX, TX

const char addr = 'B'; // network address
bool flag = false;
const int led =  1; // LED's pin

void setup() {
  mySerial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(3, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {    // check if it something available
    char receivedByte = mySerial.read();
    if (receivedByte == addr) {      
      flag = true;
    }
  }

  if (flag == true){  
    for (int i = 0; i < 10; i++) {    // flash the led 10 times
      digitalWrite(led, HIGH);
      delay(200);
      digitalWrite(led, LOW);
      delay(200);  
    }          

    flag = false;  // set the flag to false until the next valid request
  }
}
```

## Link to the codes
- [ARDUINO UNO](../files/ARDUINO_UNO_NETWORK.ino)
- [ATtiny412 board](../files/ATtiny_NETWORK.ino)

## Video

Here is a short video showing the system working.

<iframe src="https://player.vimeo.com/video/448997602" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

const int buttonPin = 0;     // pushbutton pin
const int ledPin =  1;      // LED pin

// variables will change:
int buttonState = 0;         // pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  randomSeed(300); // initializes the pseudo-random number generator
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed.
  // if it is, make the led flash with a variable frequency.
  if (buttonState == LOW) {    
    randNumber = random(200, 500);
    digitalWrite(ledPin, HIGH);
    delay(randNumber);
    digitalWrite(ledPin, LOW);
    delay(randNumber);
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}

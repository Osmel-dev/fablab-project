#include <Wire.h>
#include "Adafruit_SGP30.h"

Adafruit_SGP30 sgp;

void setup() {
  //initialize serial communications at a 9600 baud rate
  Serial.begin(9600);  
  sgp.begin();
}

void loop() {
  sgp.IAQmeasure();
  Serial.println(sgp.eCO2);
  delay(1000);
}

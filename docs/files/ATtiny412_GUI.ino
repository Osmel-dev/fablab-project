#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3); // RX, TX

const char OFFSTATE = '1';
const char ONSTATE = '2';
bool flag = false;
const int led =  1; // the number of the LED pin

void setup() {
  mySerial.begin(9600);
  pinMode(led, OUTPUT);
  //pinMode(BLUEled, OUTPUT);
  pinMode(3, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {    
    char receivedByte = mySerial.read();
    
    if (receivedByte == ONSTATE) {      
      digitalWrite(led, HIGH);
    }

    if (receivedByte == OFFSTATE){
      digitalWrite(led, LOW);    
    }
  }
}

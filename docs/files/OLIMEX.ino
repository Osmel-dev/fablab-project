// include the library code:
#include <LiquidCrystal.h>
// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 28, en = 30, d4 = 32, d5 = 33, d6 = 34, d7 = 35;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Led Pin
int led = 38;
int button1 = 44;
//int button2 = 43;

int buttonState = 0;
int currState = 0;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(button1, INPUT);
  //Pin29 is R/W pin for LCD
  pinMode(29, OUTPUT);
  digitalWrite(29, LOW);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.setCursor(2, 0); // top left
  lcd.print("Blinking led!");
  delay(1500);
}
void loop() {
  buttonState = digitalRead(button1);
  
    if (buttonState == LOW){
      currState = !currState;   
      delay(100);
    }

    if (currState == 1){
      digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(500);              // wait for a second
      digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
      delay(500);              // wait for a second

      lcd.setCursor(4, 1); // top left
      lcd.print("Led On!");      
    } else{          
      lcd.setCursor(4, 1); // top left
      lcd.print("Led Off!");
    }
    
}

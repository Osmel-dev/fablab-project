// GUI interface for the Arduino UNO serial communication. //<>//
import processing.serial.*;
import controlP5.*;

ControlP5 cp5;
int slider = 400;
Serial myPort;
String str = "";
int myColorBackground = color(255, 255, 0);

void setup() {
  size(250, 400);  
  cp5 = new ControlP5(this);
  PFont GUIfont = createFont("arial", 20);
  cp5.setFont(GUIfont);
  
  cp5.addSlider("CO2 Level").
  setPosition(50,50).
  setSize(100,300).
  setRange(400, 5000)
  .setNumberOfTickMarks(10);
  
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);
  myPort.bufferUntil('\n');
}

void draw() {  
  background(0);
    
  if (str != null){
    float val = float(str);
    cp5.getController("CO2 Level").setValue(val);
    
     // Chagge the color of the slider as a function of the CO2 level
    if (val < 1000){
      cp5.getController("CO2 Level").setColorForeground(color(0,250,0));
    } else if(val > 1000 && val < 2000){
      cp5.getController("CO2 Level").setColorForeground(color(255,255,0));
    } else {
      cp5.getController("CO2 Level").setColorForeground(color(255,0,0));
    }

  }
}

// Serial port interruption
void serialEvent(Serial port) {
  str = port.readStringUntil('\n');
} 

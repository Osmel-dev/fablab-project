// GUI interface for the Arduino UNO serial communication. //<>//
import processing.serial.*;
import controlP5.*;

//import the ControlP5 library
//Define the new GUI
ControlP5 gui;
Serial myPort;

void setup(){
//set the window size
size(200,200);
noStroke();
//Create the new GUI
gui = new ControlP5(this);
PFont GUIfont = createFont("arial", 20);
  gui.setFont(GUIfont);
//Add a Button
gui.addButton("ON")
   //Set the position of the button : (X,Y)
   .setPosition(50,30)
   //Set the size of the button : (X,Y)
   .setSize(100,50)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.RELEASE);
   
   gui.addButton("OFF1")
   //Set the position of the button : (X,Y)
   .setPosition(50,100)
   //Set the size of the button : (X,Y)
   .setSize(100,50)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
   
   String portName = Serial.list()[0];
   myPort = new Serial(this, portName, 9600);
}

public void ON(int value){
// This is the place for the code, that is activated by the buttonb
println("ON");
// Send a capital A out the serial port:
//myPort.write(2);
}

public void OFF(int value){
// This is the place for the code, that is activated by the buttonb
println("OFF");
//myPort.write(1);
}

public void controlEvent(ControlEvent theEvent) {
//Is called whenever a GUI Event happened
}

void draw(){
//Do whatever you want
}

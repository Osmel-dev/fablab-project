#include <SoftwareSerial.h>
SoftwareSerial mySerial(0, 1); // RX, TX

const char addr = 'A'; // network address
bool flag = false;
const int led =  13; // LED's pin

void setup() {
  mySerial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(3, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {    // check if it something available
    char receivedByte = mySerial.read();
    if (receivedByte == addr) {      
      flag = true;
    }
  }

  if (flag == true){  
    for (int i = 0; i < 10; i++) {    // flash the led 10 times
      digitalWrite(led, HIGH);
      delay(200);
      digitalWrite(led, LOW);
      delay(200);  
    }          

    flag = false;  // set the flag to false until the next valid request
  }
}
